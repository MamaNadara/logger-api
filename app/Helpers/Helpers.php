<?php


namespace App\Helpers;

use GuzzleHttp\Client;

class Helpers
{


    public static function callOtherApi( $params )
    {
        if (!isset($params['route'])) return 'Route not found';
        if (!isset($params['app'])) return 'App not found';

        $method             = !isset($params['method']) ? 'POST' : $params['method'];
        $data               = !isset($params['data']) ? [] : $params['data'];
        $headers            = !isset($params['headers']) ? [] : $params['headers'];
        $headers['api-key'] = env("API_KEY");

        try {
            $client = new Client();
            $url    = env($params['app'] . '_API_URL') . '/' . $params['route'];

            $res = $client->request($method, $url, ['form_params' => $data, 'headers' => $headers]);

            $result                = [];
            $result['status_code'] = $res->getStatusCode();
            $result['headers']     = $res->getHeader('content-type');
            $result['body']        = $res->getBody();

            return $result;

        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $result                = [];
            $result['status_code'] = $e->getCode();
            $result['headers']     = $e->getResponse()->getHeader('content-type');
            $result['body']        = $e->getResponse()->getBody(true);

            return $result;
        }
    }


    public static function Log($params)
    {
        if (!isset($params['description'])){
            return 'Please insert description';
        }

        $data = [
            'method' => 'POST',
            'app' => 'LOGGER',
            'route' => 'services/setLog',
            'data' => $params
        ];

        self::callOtherApi($data);
    }

}