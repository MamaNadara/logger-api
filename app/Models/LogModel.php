<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LogModel extends  Model
{
    protected $table = 'logs';

    protected $fillable = [
      'description'
    ];


    public static function setLog($params)
    {
       $data['description'] = $params['description'];
       LogModel::create($data);
    }
}