<?php

namespace App\Http\Middleware;

use App\Helpers\Helpers;
use Closure;
use GuzzleHttp\Client;

class ServiceAccessMiddleware
{

    public function handle($request, Closure $next)
    {
        $headers = [
            'external-api-key' => $request->header('api-key')
        ];

        $data = [
            'method' => $request->method(),
            'app' => 'ACCOUNTS',
            'route' => 'services/checkServiceApi',
            'headers' => $headers
        ];

        $checkAuth = Helpers::callOtherApi($data);
        $checkAuth = json_decode($checkAuth['body'], true);

        if($checkAuth['status'] == 200) {
            return $next($request);
        } else {
            return responder()->error(406, $checkAuth['error']['message'])->respond(406);
        }

    }
}
