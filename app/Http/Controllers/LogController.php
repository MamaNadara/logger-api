<?php

namespace App\Http\Controllers;

use App\Models\LogModel;
use Illuminate\Http\Request;

class LogController extends Controller
{

    public function __construct()
    {
        //
    }


    public static function setLog(Request $request)
    {
        LogModel::setLog($request);
    }

}
